export interface Wallet {
    id?: string;
    mnemonic: string;
    address: string;
    email: string;
    firstName: string;
    lastName: string;
}