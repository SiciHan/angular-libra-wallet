import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { WalletService } from "../../services/wallet.service";
import { TextAnimation } from "ngx-teximate";
import { rotateIn } from "ng-animate";

@Component({
  selector: "app-confirm",
  templateUrl: "./confirm.component.html",
  styleUrls: ["./confirm.component.css"]
})
export class ConfirmComponent implements OnInit {
  routeData: any;
  message: string;
  enterAnimation: TextAnimation = {
    animation: rotateIn,
    delay: 50,
    type: "letter"
  };
  constructor(private router: Router, private walletsvc: WalletService) {}

  ngOnInit() {
    this.routeData = this.walletsvc.confirmation;
    console.log(this.routeData);
    this.message = `${this.routeData.amount} Libra sent.`;
  }

  navigateToHistory() {
    this.router.navigate(["/history"]);
  }
}
