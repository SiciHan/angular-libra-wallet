import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import  { WalletService } from '../../services/wallet.service';

@Component({
  selector: 'app-send',
  templateUrl: './send.component.html',
  styleUrls: ['./send.component.css']
})
export class SendComponent implements OnInit {
  sendForm :FormGroup;
  fromAccount = environment.libra_account;
  mnemonic = environment.mnemonic;
  isLoading: boolean =  false;

  constructor(private fb: FormBuilder, private router: Router,
    private walletsvc: WalletService) { 
    this.sendForm = fb.group({
      destAccount: ['', [Validators.required]],
      amount: ['', [Validators.required]],
    });
  }

  ngOnInit() {
    let scanAddress = this.walletsvc.scanAddresss;
    console.log(scanAddress);
    if(typeof(scanAddress) !== 'undefined' ){
      this.sendForm.patchValue({
        destAccount: scanAddress
      });
    }
  }

  onSend(){
    this.isLoading = true;
    let destAccount = this.sendForm.get("destAccount").value;
    let amount = this.sendForm.get("amount").value;
    
    let p = {
      fromAddress: this.fromAccount,
      mnemonic: this.mnemonic,
      toAddress: destAccount,
      amount: amount
    }
    
    this.walletsvc.transfer(p).then(result=>{
      console.log(result);
      this.walletsvc.confirmSent(result);
      this.isLoading = false;
      this.router.navigate(['/confirm-send']);
    })
  }
}
