import { Component, OnInit } from '@angular/core';
import { environment} from '../../../environments/environment';
import { ClipboardService } from 'ngx-clipboard'
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-receive',
  templateUrl: './receive.component.html',
  styleUrls: ['./receive.component.css']
})
export class ReceiveComponent implements OnInit {
  value = environment.libra_account;
  constructor(private _clipboardService: ClipboardService,
    private _snackBar: MatSnackBar,
    private translator: TranslateService) { }

  ngOnInit() {
  }

  copyToClipboard(){
    this.translator.get('Copied').subscribe((res: string) => {
      let arrResult = res.split('|');
      this._clipboardService.copyFromContent(this.value);
      this._snackBar.open(arrResult[0], arrResult[1], {
        duration: 3000,
      });
    });
  }

  rotateKey(){

  }
}
