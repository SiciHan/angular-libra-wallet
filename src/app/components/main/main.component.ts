import { Component, OnInit, OnDestroy } from "@angular/core";
import { ClipboardService } from "ngx-clipboard";
import { MatSnackBar } from "@angular/material/snack-bar";
import { WalletService } from "../../services/wallet.service";
import { LocationService } from "../../services/location.service";

import { Subscription } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit, OnDestroy {
  libraAccount: any;
  amount: any;
  private walletSub: Subscription;

  constructor(
    private _clipboardService: ClipboardService,
    private _snackBar: MatSnackBar,
    private walletsvc: WalletService,
    private locSvc: LocationService
  ) {}

  ngOnDestroy() {
    this.walletSub.unsubscribe();
  }

  ngOnInit() {
    this.locSvc.getPosition().then(result => {
      console.log(result);
    });

    this.walletSub = this.walletsvc
      .getWalletByEmail()
      .snapshotChanges()
      .pipe(
        map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
        )
      )
      .subscribe(wallet => {
        console.log(wallet[0].address);
        this.libraAccount = wallet[0].address;
        this.getBalance();
      });
  }

  getBalance() {
    console.log;
    let p = {
      address: this.libraAccount
    };
    this.walletsvc.getBalance(p).then(result => {
      this.amount = result;
      console.log(this.amount);
    });
  }

  copyAccount() {
    this._clipboardService.copyFromContent(this.libraAccount);
    this._snackBar.open("Copied", "close", {
      duration: 1000
    });
  }

  refreshAmount() {
    console.log("refresh balance ...");
    this.getBalance();
  }
}
