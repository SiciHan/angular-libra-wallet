import { Component, OnInit } from "@angular/core";
import { WalletService } from "../../services/wallet.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-transaction-history",
  templateUrl: "./transaction-history.component.html",
  styleUrls: ["./transaction-history.component.css"]
})
export class TransactionHistoryComponent implements OnInit {
  transactionHistory: any;
  libraAccount = environment.libra_account;
  constructor(private walletsvc: WalletService) {}

  ngOnInit() {
    let p = {
      address: this.libraAccount
    };
    this.walletsvc.getTransactionHistory(p).then(result => {
      console.log(result);
      this.transactionHistory = result;
      console.log(this.transactionHistory);
    });
  }
}
