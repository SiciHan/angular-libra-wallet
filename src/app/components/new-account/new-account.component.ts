import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { AngularFireAuth } from "@angular/fire/auth";
import { WalletService } from "../../services/wallet.service";
import { MatSnackBar } from "@angular/material";
import { Wallet } from "../../models/Wallet";

@Component({
  selector: "app-new-account",
  templateUrl: "./new-account.component.html",
  styleUrls: ["./new-account.component.scss"]
})
export class NewAccountComponent implements OnInit {
  PASSWORD_PATTERN = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{12,}$/;
  registerForm: FormGroup;
  isLoading = false;
  isHWWalletLinked = false;

  constructor(
    private fb: FormBuilder,
    public afAuth: AngularFireAuth,
    private authService: AuthService,
    private router: Router,
    private walletSvc: WalletService,
    private snackBar: MatSnackBar
  ) {
    this.registerForm = fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.required, Validators.pattern(this.PASSWORD_PATTERN)]
      ],
      firstName: ["", [Validators.required, Validators.minLength(3)]],
      lastName: ["", [Validators.required, Validators.minLength(3)]]
    });

    this.isHWWalletLinked = window.localStorage["linkHWWallet"];
  }

  ngOnInit() {}

  registerWithEmail() {
    this.walletSvc.createWallet().then(result => {
      const formValue = this.registerForm.value;
      console.log(result);
      let w: Wallet = {
        email: this.authService.getEmail().toString(),
        mnemonic: result.mnemonic,
        address: result.address,
        firstName: formValue.firstName,
        lastName: formValue.lastName
      };
      this.walletSvc.saveWallet(w).subscribe(result => {
        console.log(result);
        let snackBarRef = this.snackBar.open("Wallet created", "done", {
          duration: 3000
        });
      });
    });
  }

  linkHWWallet() {
    console.log("Link hardware wallet");
    window.localStorage["linkHWWallet"] = true;
  }
}
